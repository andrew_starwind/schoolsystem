﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCW.Models;

namespace WebCW.Controllers
{
    public class HomeController : Controller
    {
        SchoolContext db = new SchoolContext();
        public ActionResult Index()
        {
            
            return View(db.Topics.ToList());
        }

        public ActionResult TimeTables()
        {
            var stands = db.Classes.Select(s => new SelectListItem
            {
                Value = s.Id.ToString(),
                Text = s.number + " " + s.letter
            });

            ViewBag.ClassId = new SelectList(stands, "Value", "Text");
            ViewBag.PeriodId = new SelectList(db.Periods, "Id", "name");
            return View();
        }

        public ActionResult News()
        {
            var topics = db.Topics.ToList();

            return View(topics);
        }
        public ActionResult Teachers()
        {
            var teachers = db.Teachers.ToList();

            return View(teachers);
        }

        [HttpPost]
        public JsonResult getTimetable(int ClassId, int PeriodId)
        {
            var data = db.Lessons.Where(s => s.timetable.ClassId == ClassId).Where(s => s.timetable.PeriodId == PeriodId).ToList();
            return Json(data);
        }

        public ActionResult TopicContent(int id)
        {
            var topic = db.Topics.Find(id);
            ViewBag.content = topic.Content;
            return View(topic);
        }
    }
}