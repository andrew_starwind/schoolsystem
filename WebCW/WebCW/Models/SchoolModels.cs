﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Web.Mvc;

namespace WebCW.Models
{
    public class Class
    {   
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public int number { get; set; }

        [StringLength(1)]
        public string letter { get; set; }
        public string profile { get; set; }
        
        public int? TeacherId { get; set; }
        public string photoPath { get; set; }

        [ForeignKey("TeacherId")]
        public Teacher Teacher { get; set; }

        public string imgPath { get; set; }

        public ICollection<Student> Students { get; set; }
        public ICollection<Timetable> TimeTables { get; set; }

        
    }

    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FathersName { get; set; }
        [Required]
        public int ClassId { get; set; }
        [Required]
        public string Email { get; set; }
        [ForeignKey("ClassId")]
        public Class Class { get; set; }

        public string imgPath { get; set; }
    }

    public class Teacher
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FathersName { get; set; }
        [Required]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Information { get; set; }
        [Required]
        public string Qualification { get; set; }   // типа учитель-методист или старший учитель
        [Required]
        public int Exp { get; set; } //стаж
        [Required]
        public string Subjects { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string MobilePhone { get; set; }

        public string Position { get; set; }

        public string imgPath { get; set; }
    }

    public class Period
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime begin { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime end { get; set; }
        public ICollection<Timetable> TimeTables { get; set; }
    }

    public class Lesson
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int number { get; set; }
        [Required]
        public int day { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public int TimetableId { get; set; }

        public string TeachersName { get; set; }
        [ForeignKey("TimetableId")]
        public Timetable timetable { get; set; }
    }

    public class Topic
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Theme { get; set; }
        [Required]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Content { get; set; }
        
        public DateTime published { get; set; }

        public string imgPath { get; set; }
    }


    public class Timetable
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int ClassId { get; set; }
        [Required]
        public int PeriodId { get; set; }

        [ForeignKey("ClassId")]
        public Class Class { get; set; }

        [ForeignKey("PeriodId")]
        public Period Period { get; set; }

        IEnumerable<Lesson> Lessons; 
    }

    public class LessonRequest
    {
        public string name { get; set; }
        public string teacher { get; set; }
    }
    

    public class SchoolContext : DbContext
    {
        public DbSet<Class> Classes { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Timetable> Timetables { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Lesson> Lessons { get; set; }

        public SchoolContext()
        : base("name=SchoolContext")
        {
            Database.SetInitializer<SchoolContext>(new CreateDatabaseIfNotExists<SchoolContext>());
        }
    }
}